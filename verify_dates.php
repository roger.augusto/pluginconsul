<?php





class VerifyDates{



    private $periodo;

    private $day_week;

    private $data_periodo;

    private $meta_value_next_remember;



    public function __construct($periodo = '', $day_week = '', $data_periodo = '', $meta_value_next_remember = ''){

        $this->periodo = $periodo;

        $this->day_week = $day_week;

        $this->data_periodo = $data_periodo;

        $this->meta_value_next_remember = $meta_value_next_remember;

    }



    /**

     * Recebe como parametro String com periodo marcado pelo usuario e retorna a data

     * pra ser enviado o lembrete

     * @param string $periodo

     * @return date

    */

    function verifyPeriod($periodo){



        $meta_value_next_remember;

        // verifica valor que foi marcado e calcula a data do envio de lembrete

        switch ($periodo) {

            // case 'agora':

            //     $meta_value_next_remember = date("Y-m-d");

            // break;

            case 'dia':

                $meta_value_next_remember = date("Y-m-d" ,mktime(0,0,0,date("m"),date("d")+1,date("Y")));

            break;

            case 'semana':

                $meta_value_next_remember = date("Y-m-d" ,mktime(0,0,0,date("m"),date("d")+7,date("Y")));

            break;

            case 'quinzena':

                $meta_value_next_remember = date("Y-m-d" ,mktime(0,0,0,date("m"),date("d")+15,date("Y")));

            break;

            case 'mes':

                $meta_value_next_remember = date("Y-m-d" ,mktime(0,0,0,date("m")+1,date("d"),date("Y")));

            break;

        }

        return $meta_value_next_remember;

    }



    /**

     * Função que vai verificar a data atual com a data que o usuario selecionou e retornar a proxima data de acordo com o periodo

     * @param string @day_week

     * @param string @data_periodo

     * @return date $day

    */

    function verifyDayWeek($day_week, $data_periodo){



        // verifica valor que foi marcado como dia da semana e calcula a data do envio de lembrete

        switch ($day_week) {

            case 'segunda':

                $week_day = strftime("%A",strtotime($data_periodo));

                if( $week_day == 'Monday'){

                    return $data_periodo;

                }else{

                    $day = new DateTime($data_periodo);

                    $day->modify( 'next monday' );

                    return $day->format('Y-m-d');

                }

            break;

            case 'terca':

                $week_day = strftime("%A",strtotime($data_periodo));

                if($week_day == 'Tuesday'){

                    return $data_periodo;

                }else{

                    $day = new DateTime($data_periodo);

                    $day->modify( 'next tuesday' );

                    return $day->format('Y-m-d');

                }

            break;

            case 'quarta':

                $week_day = strftime("%A",strtotime($data_periodo));

                if($week_day == 'Wednesday'){

                    return $data_periodo;

                }else{

                    $day = new DateTime($data_periodo);

                    $day->modify( 'next wednesday' );

                    return $day->format('Y-m-d');

                }

            break;

            case 'quinta':

                $week_day = strftime("%A",strtotime($data_periodo));

                if($week_day == 'Thursday'){

                    return $data_periodo;

                }else{

                    $day = new DateTime($data_periodo);

                    $day->modify( 'next thursday' );

                    return $day->format('Y-m-d');

                }

            break;

            case 'sexta':

                $week_day = strftime("%A",strtotime($data_periodo));

                if($week_day == 'Friday'){

                    return $data_periodo;

                }else{

                    $day = new DateTime($data_periodo);

                    $day->modify( 'next friday' );

                    return $day->format('Y-m-d');

                }

            break;

        }

    }



    /**

     * Função que vai verificar  se a data com que o usuario preencheu é a data de hoje caso seja ele vai modificar

     * a data para a proxima data de acordo com o periodo que o usuario selecionou

     * @param date $meta_value_next_remember

     * @param string $periodo

     * @return date

     */

    function verifyDateToday($meta_value_next_remember,$periodo){

        

        $dateToday = date("Y-m-d");

        $dateTodayCompare =  strftime("%A",strtotime($meta_value_next_remember));

        $date = strftime("%A",strtotime($meta_value_next_remember));



        if($dateTodayCompare == $date){

            return $this->verifyPeriod($periodo);

        }else{

            return $meta_value_next_remember;

        }

    }

}

