<?php

/**

 * Plugin Name: Remember Order

 * Plugin URI: http://www.ecomd.com.br

 * Description: Essa extensão gera um lembrete de recompra com periodicidade, enviado ao e-mail do cliente.

 * Authors: Guilherme Domingues do Prado, Roger Augusto Santos Soares

 * Author URI: http://www.ecomd.com.br

 * Text Domain: one-click-remember-reorder

 * Version: 1.0

 * Tested up to: 5.4

 *

 */



require_once('verify_dates.php');



 if (!defined('ABSPATH'))

{

    exit(); // Exit if accessed directly

}



// Adicionar arquivos css e js do fancybox

function my_scripts_form_remember()

{



    wp_enqueue_script('fancyboxjs', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js');

    wp_enqueue_style('fancyboxcss', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css');



}

add_action('wp_enqueue_scripts', 'my_scripts_form_remember');



// Adicionar js personalizado

function customJsScript()

{

    echo "

    <script>

      jQuery(function(){

            jQuery('a.woocommerce-button.button.form_btn_remember').attr('data-fancybox','').attr('data-src','#hidden-content');

            jQuery('a.woocommerce-button.button.form_btn_remember' ).click(function() {

              var id = jQuery(this).attr('href').split('http://')[1];

              jQuery('#hidden_order_id').val(id);

            });



            var selectPeriodo = document.getElementById('periodo');

            var diaSemana = document.getElementById('dia_semana');

            var dayWeek = document.getElementById('day_week');

            selectPeriodo.addEventListener('change', function(){



                if(selectPeriodo.value == 'dia'){

                    diaSemana.style.display = 'none';

                    diaSemana.required = false;

                    dayWeek.style.display = 'none';

                }else{

                    diaSemana.style.display = 'block';

                    diaSemana.required = true;

                    dayWeek.style.display = 'block';

                }

            });



            jQuery('a.woocommerce-button.button.form_btn_remember_off').attr('data-fancybox', '').attr('data-src','#hidden-content-disable');

            var id_order;

            jQuery('a.woocommerce-button.button.form_btn_remember_off' ).click(function() {

                var id = jQuery(this).attr('href').split('http://')[1];

                jQuery('#hidden_order_disable').val(id);

                id_order = jQuery('#hidden_order_disable').val();

            });



            jQuery('.button_disable').click(function(){



                jQuery.ajax({

                  method: 'POST',

                  url: '/wp-admin/admin-ajax.php',

                  data: {

                      'action': 'disableRemember',

                      'id': id_order

                  },

                  success: function(response){

                        console.log('Desativado com Sucesso!');

                  }

                });

            });

        });

    </script>

    ";

}



//Adiciona CSS personalizado

function customCSS(){

    echo '

        <style>
            a.woocommerce-button.button.form_btn_remember {
                background: #f19722;
            }

            a.woocommerce-button.button.form_btn_remember_off {
                background: #e11830;
            }

            .button_disable{
                background: #e11830;
            }

            input#ativar-lembrete{
                background-color: #007521 !important;
                color: #f7f7f7;
            }

            .fancybox-close-small{
                position: absolute !important;
                color: black !important;
                background: whitesmoke !important;
            }
        </style>

    ';

}



add_action('wp_footer', 'customJsScript');

add_action('wp_head', 'customCSS');



function ro_create_custom_post_type()

{

    /*

     * Os rótulos $ descrevem como o tipo de postagem aparece.



    */

    $labels = array(

        'name' => 'Remembers', // Plural name

        'singular_name' => 'Remember'

        // Singular name

        

    );



    /*

* O parâmetro $supports descreve o que o tipo de postagem suporta

    */

    $supports = array(

        'title', // Post title

        'author', // Allows showing and choosing author

        'custom-fields' // Supports by custom fields

        

    );



    /*

*   O parâmetro $args contém parâmetros importantes para o tipo de postagem personalizada

    */

    $args = array(

        'labels' => $labels,

        'description' => 'Post type post date remember', // Description

        'supports' => $supports,

        'hierarchical' => false, // Permite a categorização hierárquica, se definido como falso, o tipo de postagem personalizada se comportará como uma postagem, caso contrário, se comportará como uma página

        'public' => true, // Torna o tipo de postagem público

        'show_ui' => true, // Exibe uma interface para este tipo de postagem

        'show_in_menu' => true, // Exibido no Menu Admin (painel esquerdo)

        'show_in_nav_menus' => true, // Exibe em Aparência -> Menus

        'show_in_admin_bar' => true, // Exibido na barra preta de administração

        'menu_position' => 5, // O número da posição no menu esquerdo

        'menu_icon' => true, // O URL do ícone usado para este tipo de postagem

        'can_export' => true, // Permite a exportação de conteúdo usando Ferramentas -> Exportar

        'has_archive' => true, // Ativa o tipo de arquivo de postagem (por mês, data ou ano)

        'exclude_from_search' => false, // Exclui postagens deste tipo na página de resultados de pesquisa de front-end se definido como verdadeiro, inclui-as se definido como falso

        'publicly_queryable' => true, // Permite que as consultas sejam realizadas na parte do front-end se definido como verdadeiro

        'capability_type' => 'post', // Permite ler, editar, deletar como “Post”

        'capabilities' => array(

            'create_posts' => 'do_not_allow', // Removes support for the "Add New" function ( use 'do_not_allow' instead of false for multisite set ups )

        ) ,

    );



register_post_type('remember', $args); //Crie um tipo de postagem com o slug "lembrar" e os argumentos em $ args.

    

}



add_action('init', 'ro_create_custom_post_type');



//Função executada via Ajax, responsavel por pegar o id da order que o usuario que desativar

//enviado executar uma query, trazer os dados desse post pegar o ID e deletar do banco de dados

function disableRemember(){



    if(isset($_REQUEST['id'])){

        $order_id_disable = $_REQUEST['id'];



        $ro_args = array(

            'post_type' => 'remember',

            'meta_key' => '_ro_id',

            'meta_value' => $order_id_disable,

            'compare' => '='

        );



        $query = new WP_Query($ro_args);

        $post_id_disable;

        foreach ($query as $key => $value){

            if($key == 'post'){

                $post_id_disable = $value->ID;

            }

        }

        wp_delete_post($post_id_disable);

    }

}

//Actions para adicionar a função disableRemember ao arquivo ajax do WordPress

add_action( 'wp_ajax_nopriv_disableRemember', 'disableRemember' );

add_action( 'wp_ajax_disableRemember', 'disableRemember' );





// função para buscar valores do formulário

add_action('init','add_get_val');

//Função responsavel por pegar valores de lembrete definidos pelo usuario

//verificar e executar as validações e salvar no banco esses valores

function add_get_val() { 



    if(isset($_GET['periodo']) and isset($_GET['hidden_order_id']))

    {

        $periodo = $_GET['periodo'];

        $order_id = $_GET['hidden_order_id'];

        $diaSemana = $_GET['dia_semana'];



        $ro_args = array(

            'post_type' => 'remember',

            'meta_key' => '_ro_id',

            'meta_value' => $order_id,

            'compare' => '='

        );



        $query = new WP_Query($ro_args);

        $post_id_disable;

        foreach ($query as $key => $value){

            if($key == 'post'){

                $post_id_disable = $value->ID;

            }

        }



        //buscar ID do post que vai ser o post_id na tabela wp_postmeta

        $postmeta_id_update;

        if(array_key_exists('post', $query)){

            foreach ($query as $key => $value){

                if($key == 'post'){

                   $postmeta_id_update = $value->ID;

                }

            }

        }



        if(array_key_exists('post', $query)){

            //instanciar classe que verifica as datas

            $verify_dates = new VerifyDates;



            if($periodo == 'dia'){

                $date_period = $verify_dates->verifyPeriod($periodo);

                update_post_meta($postmeta_id_update,'_next_remember',$date_period);

            }else{

                $date_period = date("Y-m-d");

                $date_next = strval($date_period);

                $new_next_remember = $verify_dates->verifyDayWeek($diaSemana, $date_next);

                $new_next = strval($new_next_remember);



                if($date_next == $new_next){

                    $new_next_remember = $verify_dates->verifyPeriod($periodo);

                    update_post_meta($postmeta_id_update,'_next_remember',$new_next_remember);

                }elseif($periodo == 'dia'){

                    $new_next_remember = $verify_dates->verifyPeriod($periodo);

                    update_post_meta($postmeta_id_update,'_next_remember',$new_next_remember);

                }else{

                    update_post_meta($postmeta_id_update,'_next_remember',$new_next_remember);

                }

                

            }



            $new_next_remember_day = $diaSemana;

            update_post_meta($postmeta_id_update,'_next_remember_day_week',$new_next_remember_day);



            update_post_meta($postmeta_id_update,'_period_remember', $periodo);

        }else{



            //insere na tabela wp_post 

            $custom_post = wp_insert_post(array(

                'post_title'=>'Lembrete de Recompra do pedido ' . $order_id,

                'post_type'=>'remember',

                'post_status' => 'publish',

                'ping_status' => 'open'

            ));



            //get email do usuario logado

            $user = wp_get_current_user();

            $user_id;

            foreach ($user as $key => $value) {

                if($key == 'ID'){

                    $user_id = $value;

                }

            }



            //email de pagamento ou cobrança

            $user_email = get_user_meta($user_id, 'billing_email', true);



            //get id do post criado

            $id = $custom_post;

            $result = get_post($id);

            $post_id;

            $meta_value_date;

            foreach ($result as $key => $value) {

                if($key == 'ID'){

                    $post_id = $value;

                }

                if($key == 'post_date'){

                    $meta_value_date = $value;

                }

            }



            //instanciar classe que verifica as datas

            $verify_dates = new VerifyDates;



            $meta_key_date = '_order_date_made';



            // ro = remember_order

            $meta_key_order =  '_ro_id';

            $meta_value_order = $order_id;



            $meta_key_email = '_user_mail';

            $meta_value_mail = $user_email;



            $meta_key_periodo = '_period_remember';

            $meta_value_periodo = $periodo;



            $meta_key_next_remember = '_next_remember';

            $meta_value_next_remember;



            if($periodo == 'dia'){

                $date_period = $verify_dates->verifyPeriod($periodo);

                $meta_value_next_remember = $date_period;

            }else{

                $date_period = date("Y-m-d");

                $date_next = strval($date_period);

                $meta_value_next_remember_validation = $verify_dates->verifyDayWeek($diaSemana, $date_next);

                $meta_value_next_remember = $verify_dates->verifyDateToday($meta_value_next_remember_validation, $periodo);

            }

            

            // if($periodo == 'agora'){

            //     $meta_value_next_remember = $verify_dates->verifyPeriod($periodo);

            // }else{

            //     $meta_value_next_remember = ($periodo != 'dia') ? $verify_dates->verifyDayWeek($diaSemana, $date_next) : $date_period ;

            // }



            $meta_key_remember_day_week = '_next_remember_day_week';

            $meta_value_remember_day_week = $diaSemana;

            

            //salvando no banco de dados wp_postmeta

            add_post_meta($post_id, $meta_key_date, $meta_value_date);

            add_post_meta($post_id, $meta_key_order, $meta_value_order);

            add_post_meta($post_id, $meta_key_email, $meta_value_mail);

            add_post_meta($post_id, $meta_key_periodo, $meta_value_periodo);

            add_post_meta($post_id, $meta_key_next_remember, $meta_value_next_remember);

            add_post_meta($post_id, $meta_key_remember_day_week, $meta_value_remember_day_week);

        }

    }

}



// filtro para adicionar eventos aos botões dos pedidos

add_filter('woocommerce_my_account_my_orders_actions', 'sv_add_my_account_order_actions', 10, 2);

// função para chamar o formulário

function sv_add_my_account_order_actions($actions, $order)

{



    $ro_args = array(

        'post_type' => 'remember',

        'meta_key' => '_ro_id',

        'meta_value' => $order->id,

        'compare' => '='

    );



    $query = new WP_Query($ro_args);





    if(array_key_exists('post', $query)){

        $name_button = 'Atualizar Lembrete de Compra';



        $actions['form_btn_remember'] = array(

            // ajuste o URL conforme necessário

            'url' => "" . $order->id,

            'name' => $name_button

        );

    

        $actions['form_btn_remember_off'] = array(

            // ajuste o URL conforme necessário

            'url' => "" . $order->id,

            'name' => 'Desativar Lembrete'

        );



        return $actions;





    }else{

        $name_button = 'Adicionar Lembrete de Recompra';



        $actions['form_btn_remember'] = array(

            // ajuste o URL conforme necessário

            'url' => "" . $order->id,

            'name' => $name_button

        );



        return $actions;

    }

}



// HTML do formulário no popup

add_action('the_post', 'verifyPage' , 10, 2);



    //Função que contem o html do Pop-ub exibido ao usuario

    function verifyPage($post, $order){

        if(!is_admin() && get_the_ID() == 10){

                

            $post->post_content = '<div style="display:none;" id="hidden-content">

                                        <h2>Lembrete de Recompra</h2>

                                        <p>Selecione o período em que deseje receber a notificação de lembrete para esse pedido </p>

                                        <form action="" method="get">

                                            <label for="periodo" style="display:block;">Período:</label> 

                                            <select id="periodo" name="periodo" required style="display:block;">

                                                <option value=""> </option>

                                                <option value="dia">Diário</option>

                                                <option value="semana">Semanal</option>

                                                <option value="quinzena">Quinzenal</option>

                                                <option value="mes">Mensal</option>

                                            </select>

                                            <br/>

                                            <label for="dia-semana" id="day_week" style="margin-top:5px;" >Dia da semana:</label> 

                                            <select id="dia_semana" name="dia_semana" required>

                                                <option value=""> </option>

                                                <option value="segunda">Segunda-feira</option>

                                                <option value="terca">Terça-Feira</option>

                                                <option value="quarta">Quarta-Feira</option>

                                                <option value="quinta">Quinta-Feira</option>

                                                <option value="sexta">Sexta-Feira</option>

                                            </select>

                                            <label style="margin-top:5px;" ><strong>OBS: Os emails são enviados entre as 08hrs ás 10:30hrs da manhã.</strong></label> 

                                            <input type="hidden" id="hidden_order_id" name="hidden_order_id" value="">

                                            <p></p>

                                            <input id="ativar-lembrete" type="submit" value="Ativar Lembrete">

                                        </form>

                                    </div>' . $post->post_content;



            $post->post_content =   '<div style="display:none;" id="hidden-content-disable">

                                        <h2>Lembrete de Recompra</h2>

                                        <p>Deseja realmente desativar lembrete de recompra?</p>

                                        <form action="" method="get">

                                            <input type="hidden" id="hidden_order_disable" name="hidden_order_disable" value="">

                                            <p></p>

                                            <input class="button_disable" type="submit" value="Desativar Lembrete">

                                        </form>

                                    </div>' . $post->post_content;

        }

    }

?>