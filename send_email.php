<?php



$path = preg_replace('/wp-content.*$/','',__DIR__);

include($path.'wp-load.php');



/**

 * Classe responsavel pelo envio de email de lembrete de recompra

 */

class SendMail{



    private  $user_email;

    private  $order_id;



    public function __construct($user_email, $order_id){

        $this->user_email = $user_email;

        $this->order_id = $order_id;

    }



    //Função responsavel por enviar os emails 

    function sendEmailRemember($user_email, $order_id){



        //função para adicionar cabeçalho do email o tipo html, para reconhecer as tags

        add_filter( 'wp_mail_content_type',function ($content_type){

            return 'text/html';

        });

        

        $subject = 'Está na hora de você refazer o pedido '. $order_id;

        

        $body    = '

        <!DOCTYPE html>

        <html lang="pt-BR" style="height: 100%; position: relative;">

            <head>

                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

                <title>Supermercado Cooperativa Consul</title>

            </head>

            <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" class="kt-woo-wrap order-items-light k-responsive-normal title-style-none email-id-" style="height: 100%; position: relative; background-color: #f7f7f7; margin: 0; padding: 0;">

                <div id="wrapper" dir="ltr" style="background-color: #f7f7f7; margin: 0; padding: 70px 0 70px 0; width: 100%; padding-top: 70px; padding-bottom: px; -webkit-text-size-adjust: none;">

                    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">

                        <tr>

                            <td align="center" valign="top">

                                                        <table border="0" cellpadding="0" cellspacing="0" width="600" id="template_container" style="background-color: #ffffff; overflow: hidden; border-style: solid; border-width: 1px; border-right-width: px; border-bottom-width: px; border-left-width: px; border-color: #dedede; border-radius: 3px; box-shadow: 0 1px 4px 1px rgba(0,0,0,0.1);">

                                    <tr>

                                        <td align="center" valign="top">

                                                                                    <table id="template_header_image_container" style="width: 100%; background-color: transparent;">

                                                    <tr id="template_header_image">

                                                        <td align="center" valign="middle">

                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="template_header_image_table">

                                                                <tr>

                                                                    <td align="center" valign="middle" style="text-align: center; padding-top: 0px; padding-bottom: 0px;">

                                                                        <p style="margin-bottom: 0; margin-top: 0;"><a href="https://lojacooperativaconsul.com.br" target="_blank" style="font-weight: normal; color: #4faf68; display: block; text-decoration: none;"><img src="https://cdn.lojacooperativaconsul.com.br/wp-content/uploads/2020/04/logo-loja-consul.png" alt="Supermercado Cooperativa Consul" width="160" style="border: none; display: inline; font-weight: bold; height: auto; outline: none; text-decoration: none; text-transform: capitalize; font-size: 14px; line-height: 24px; width: 100%; max-width: 160px;"></a></p>															</td>

                                                                </tr>

                                                            </table>

                                                        </td>

                                                    </tr>

                                                </table>

                                                                                <!-- Header -->

                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="template_header" style="border-bottom: 0; font-weight: bold; line-height: 100%; vertical-align: middle; font-family: Helvetica Neue, Helvetica, Roboto, Arial, sans-serif; background-color: #4faf68; color: #ffffff;">

                                                <tr>

                                                    <td id="header_wrapper" style="padding: 36px 48px; display: block; text-align: center; padding-top: 20px; padding-bottom: 20px; padding-left: 48px; padding-right: 48px;">

                                                                                                        <h1 style="margin: 0; text-align: center; font-size: 24px; line-height: 40px; font-family: Helvetica Neue, Helvetica, Roboto, Arial, sans-serif; font-style: normal; font-weight: 300; color: #ffffff;">Consul – cada vez mais perto de você!</h1>

                                                                                                    </td>

                                                </tr>

                                            </table>

                                            <!-- End Header -->

                                        </td>

                                    </tr>

                                    <tr>

                                        <td align="center" valign="top">

                                            <!-- Body -->

                                            <table border="0" cellpadding="0" cellspacing="0" width="600" id="template_body">

                                                <tr>

                                                    <td valign="top" id="body_content" style="background-color: #ffffff; padding-top: 30px; padding-bottom: 0px;">

                                                        <!-- Content -->

                                                        <table border="0" cellpadding="20" cellspacing="0" width="100%">

                                                            <tr>

                                                                <td valign="top" style="padding: 0px 48px 0; padding-left: 40px; padding-right: 40px;">

                                                                    <div id="body_content_inner" style="color: #636363; text-align: left; font-size: 14px; line-height: 24px; font-family: Helvetica Neue, Helvetica, Roboto, Arial, sans-serif; font-weight: 400;">

        <p style="margin: 0 0 16px;">Olá, estamos passando para lembrar você que está na hora de refazer o pedido ' .$order_id . '. </p>

        <p style="margin: 0 0 16px;">Basta clicar no link abaixo, acessar o pedido em sua conta e clicar na opção "Recomprar Agora". Simples Assim</p>

        <div style="display: flex; justify-content: center; align-items: center; padding: 10px;">

            <a href="https://www.lojacooperativaconsul.com.br/minha-conta/view-order/'.$order_id.'/?redirect_to=https%3A%2F%2Fwww.lojacooperativaconsul.com.br%2Fminha-conta%2Fview-order%2F'.$order_id.'"><button href="https://www.lojacooperativaconsul.com.br/minha-conta/view-order/'.$order_id.'/?redirect_to=https%3A%2F%2Fwww.lojacooperativaconsul.com.br%2Fminha-conta%2Fview-order%2F'.$order_id.'" style="height: 50px; cursor:pointer; border-radius: 5px; border: none; font-size: 15px; font-weight: bold; background-color: #4faf68; margin: auto;">Recomprar Agora</button></a>

        </div>

                                                                    </div>

                                                                </td>

                                                            </tr>

                                                        </table>

                                                        <!-- End Content -->

                                                    </td>

                                                </tr>

                                            </table>

                                            <!-- End Body -->

                                        </td>

                                    </tr>

                                    <tr>

                                        <td align="center" valign="top">

                                                                            </td>

                                    </tr>

                                </table> <!-- End template container -->

                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="template_footer_container">

                    <tr>

                        <td valign="top" align="center">

                            <table border="0" cellpadding="10" cellspacing="0" width="600" id="template_footer">

                                <tr>

                                    <td valign="top" id="template_footer_inside" style="padding: 0; padding-top: 30px; padding-bottom: 0px; padding-left: 129px; padding-right: 129px;">

                                        <table border="0" cellpadding="10" cellspacing="0" width="100%">

                                                                                    <tr>

                                                    <td valign="top" style="padding: 0;">

                                                        <table id="footersocial" border="0" cellpadding="10" cellspacing="0" width="100%" style="border-bottom-width: 0px; border-bottom-color: #e8e8e8; border-bottom-style: solid;">

                                                            <tr>

                                                                                                                            <td valign="middle" style="padding: 0; padding-top: 0px; padding-bottom: 25px; text-align: center; width: 33.33%;">

                                                                    <a href="https://www.instagram.com/consul.cooperativa/" class="ft-social-link" style="font-weight: normal; color: #848484; font-family: Helvetica Neue, Helvetica, Roboto, Arial, sans-serif; display: block; text-decoration: none;">

                                                                    <img alt="instagram" src="https://lojacooperativaconsul.com.br/wp-content/plugins/kadence-woocommerce-email-designer/assets/images/gray/instagram.png" width="24" style="border: none; font-weight: bold; height: auto; outline: none; text-decoration: none; text-transform: capitalize; font-size: 14px; line-height: 24px; max-width: 24px; width: 100%; display: inline-block; vertical-align: bottom;">															<span class="ft-social-title" style="line-height: 24px; padding-left: 5px; font-size: 12px; font-weight: 200;">Instagram</span>

                                                                    </a>

                                                                    </td>

                                                                                                                                <td valign="middle" style="padding: 0; padding-top: 0px; padding-bottom: 25px; text-align: center; width: 33.33%;">

                                                                    <a href="https://www.facebook.com/consulcooperativa/" class="ft-social-link" style="font-weight: normal; color: #848484; font-family: Helvetica Neue, Helvetica, Roboto, Arial, sans-serif; display: block; text-decoration: none;">

                                                                    <img alt="facebook" src="https://lojacooperativaconsul.com.br/wp-content/plugins/kadence-woocommerce-email-designer/assets/images/gray/facebook.png" width="24" style="border: none; font-weight: bold; height: auto; outline: none; text-decoration: none; text-transform: capitalize; font-size: 14px; line-height: 24px; max-width: 24px; width: 100%; display: inline-block; vertical-align: bottom;">															<span class="ft-social-title" style="line-height: 24px; padding-left: 5px; font-size: 12px; font-weight: 200;">Facebook</span>

                                                                    </a>

                                                                    </td>

                                                                                                                                <td valign="middle" style="padding: 0; padding-top: 0px; padding-bottom: 25px; text-align: center; width: 33.33%;">

                                                                    <a href="https://www.youtube.com/channel/UCCusFZODDU4wn42Aq9QZCjQ" class="ft-social-link" style="font-weight: normal; color: #848484; font-family: Helvetica Neue, Helvetica, Roboto, Arial, sans-serif; display: block; text-decoration: none;">

                                                                    <img alt="youtube" src="https://lojacooperativaconsul.com.br/wp-content/plugins/kadence-woocommerce-email-designer/assets/images/gray/youtube.png" width="24" style="border: none; font-weight: bold; height: auto; outline: none; text-decoration: none; text-transform: capitalize; font-size: 14px; line-height: 24px; max-width: 24px; width: 100%; display: inline-block; vertical-align: bottom;">															<span class="ft-social-title" style="line-height: 24px; padding-left: 5px; font-size: 12px; font-weight: 200;">Youtube</span>

                                                                    </a>

                                                                    </td>

                                                                                                                        </tr>

                                                        </table>

                                                    </td>

                                                </tr>

                                                                                <tr>

                                                <td valign="top" style="padding: 0;">

                                                    <table border="0" cellpadding="10" cellspacing="0" width="100%">

                                                        <tr>

                                                            <td colspan="2" valign="middle" id="credit" style="padding: 0; border: 0; line-height: 125%; padding-left: 0px; padding-right: 0px; text-align: center; font-size: 14px; font-family: Helvetica Neue, Helvetica, Roboto, Arial, sans-serif; font-weight: 400; color: #555555; padding-top: 0px; padding-bottom: 0px;">

                                                                <p style="color: #555555;"><b>Consul – cada vez mais perto de você!</b></p>

        <p style="color: #555555;">Para mais atualizações, acesse os seus <a href="https://www.lojacooperativaconsul.com.br/minha-conta/orders/" style="font-weight: normal; text-decoration: underline; color: #555555;">pedidos</a>.<br>

        Dúvidas? Acesse a nossa <a href="https://www.lojacooperativaconsul.com.br/contato/" style="font-weight: normal; text-decoration: underline; color: #555555;">central de atendimento</a>.</p>

                                                            </td>

                                                        </tr>

                                                    </table>

                                                </td>

                                            </tr>

                                        </table>

                                    </td>

                                </tr>

                            </table>

                        </td>

                    </tr>

                </table>

                                            <table class="gmail-app-fix" width="100%" border="0" cellpadding="0" cellspacing="0">

                                        <tr>

                                            <td>

                                                <table cellpadding="0" cellspacing="0" border="0" align="center" width="600">

                                                    <tr>

                                                        <td cellpadding="0" cellspacing="0" border="0" height="1" style="line-height: 1px; min-width: 200px;">

                                                        </td>

                                                        <td cellpadding="0" cellspacing="0" border="0" height="1" style="line-height: 1px; min-width: 200px;">

                                                        </td>

                                                        <td cellpadding="0" cellspacing="0" border="0" height="1" style="line-height: 1px; min-width: 200px;">

                                                        </td>

                                                    </tr>

                                                </table>

                                            </td>

                                        </tr>

                                    </table>

                                                    </td>

                        </tr>

                    </table>

                </div>

            </body>

        </html>

        ';

        

        wp_mail( $user_email, $subject, $body );

        

        // Reset content-type para evitar conflicts

        remove_filter( 'wp_mail_content_type', 'wpdocs_set_html_mail_content_type' );

    }

}

