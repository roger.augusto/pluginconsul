<?php

$path = preg_replace('/wp-content.*$/','',__DIR__);

include($path.'wp-load.php');

require_once('send_email.php');
require_once('verify_dates.php');

/**
 * Classe responsavel por verificar a data dos lembretes de recompra de acordo com as datas salvas no banco de dados,
 * busca no banco de dados os post_type do tipo remember e executa as verificações de acordo com as datas para enviar email 
 * somente nas datas corretas e atualizar os lembretes para as proximas datas
 */
class VerifySendEmail{

    function verifySendEMailRemember(){
        $ro_args = array(
            'post_type' => 'remember'
        );

        //query que busca todos os posts do tipo remember
        $query = new WP_Query($ro_args);

        //array com todos ids de posts
        $arr = array();
        if(array_key_exists('posts', $query)){
            //percorrendo array de posts
            foreach ($query as $key => $value){
                if($key == 'posts'){
                    for ($i=0; $i < count($value) ; $i++) { 
                        $id = $value[$i]->ID;
                        //inserindo id do post no array $arr[]
                        array_push($arr,$id);
                    }
                }
            }
        }

        //função que percorre array com ids dos posts remember e verifica data 
        // para envio do lembrete de recompra
        foreach ($arr as $key => $value) {
            $post_metas = get_post_meta($value);
            foreach ($post_metas as $k => $v) {

                $periodo;
                if($k == '_period_remember'){
                    $periodo = $v[0];
                }

                $user_email;
                if($k == '_user_mail'){
                    $user_email = $v[0];
                }

                $order_id;
                if($k == '_ro_id'){
                    $order_id = $v[0];
                }

                $dia;
                if($k == '_next_remember_day_week'){
                    $dia = $v[0];
                }

                if($k == '_next_remember'){
                    $dateRemember = $v[0];
                    $todayDate = date('Y-m-d');

                    if($dateRemember == $todayDate){

                        $send_mail = new SendMail($user_email, $order_id);
                        $send_mail->sendEmailRemember($user_email, $order_id);

                        $verify_dates = new VerifyDates;
                        $new_next_remember = $verify_dates->verifyPeriod($periodo);
                        update_post_meta($value,'_next_remember',$new_next_remember);

                    }
                }
            }
        }
    }
}