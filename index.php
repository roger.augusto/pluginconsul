
<?php

$path = preg_replace('/wp-content.*$/','',__DIR__);
include($path.'wp-load.php');

require_once('verify_email.php');
$verify_email = new VerifySendEmail;

//Verifica se foi enviado um parametro via GET, para ser executado a verificação
// de acordo com a data do envio de lembrete de recompra
if(isset($_GET['send_email'])){

    $enviar = $_GET['send_email'];

    if($enviar == 'enviar'){
        $verify_email->verifySendEMailRemember();
    }
}